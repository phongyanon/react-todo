import axios from 'axios'

let backEnd = 'http://localhost:8000/api/v1'

export const Api = axios.create({
    baseURL: backEnd,
    timeout: 15000,
    // withCredentials: false,
});

export default Api;