import { createBrowserHistory } from 'history'
import { applyMiddleware, compose, createStore } from 'redux'
import thunkMiddleware from 'redux-thunk'
import { routerMiddleware } from 'connected-react-router'
import { loadingBarMiddleware } from 'react-redux-loading-bar'

import rootReducer from './reducers'

export const history = createBrowserHistory()

export const configStore = preloadedState => {
    /**
     * compose is higher order function instead having to write.
     * func1(func2(func3))
     * 
     * when use compose instead.
     * compose(func1, func2, func3)
     */
    const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose

    const store = createStore(
       rootReducer(history),
       preloadedState,
       composeEnhancer(
           applyMiddleware(routerMiddleware(history), thunkMiddleware, loadingBarMiddleware())
       )
    )

     return store
}