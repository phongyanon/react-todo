let initState = {
    authenticate: false,
    userinfo: undefined,
}

const auth = (state = initState, action) => {
    switch(action.type){
        case 'SIGNIN':
            return {...state, authenticate: true}
        case 'SIGNOUT':
            return {...state, authenticate: false}
        case 'USERINFO':
            return {...state, userinfo: action.userinfo}
        case 'SIGNIN_FAILURE':
            return {}
        default:
            return state
    }
}

export default auth

/**
 * ...args is rest parameter. this is parameter before assign.
 * exmaple 
 * func(...args){
 *  for (i in args) console.log(args[i])
 * }
 * func(1, 2, 3)
 * 
 * output => 1 2 3
 * 
 * 
 */