let initState = {
    todos: [],
    cursorTodo: 0,
    deleteTodoId: false,
    currentTodo: false,
    currentPage: localStorage.getItem('todo-currentPage') || 'overview', // overview, active, complete, new
    viewMode: localStorage.getItem('todo-viewMode') || 'list', // list, card, paper
    deleteModalIsOpen: false,
    importModalIsOpen: false,
    exportModalIsOpen: false
}
let todoList

const todo = (state = initState, action) => {
    switch(action.type){
        case 'ADD':
            todoList = state.todos.push(action.todo)
            return {...state, todos: state.todos}
        case 'DELETE':
            todoList = state.todos.filter((item) => {
                return item.id.toString() !== action.todo.id.toString()
            })
            return {...state, todos: todoList}
        case 'UPDATE':
            todoList = state.todos.map((item) => {
                if(item.id === action.id){
                    item = action.todo
                }
                return item
            })
            return {...state, todos: todoList}
        case 'GET':
            let todo = state.todos.filter((item) => {
                return item.id === action.id
            })
            return {...state, currentTodo: todo[0]}
        case 'GETALL':
            return {...state, todos: state.todos}
        case 'SETALL':
            return {...state, todos: action.todos}
        case 'SET_CURSOR':
            return {...state, cursorTodo: action.cursorTodo}
        case 'GET_CURSOR':
            return {...state, cursorTodo: state.cursorTodo}
        case 'FAILURE':
            return {}
        // filter view mode
        case 'SET_CURRENT_TODO':
            return {...state, currentTodo: action.currentTodo}
        case 'GET_CURRENT_TODO':
            return {...state, currentTodo: state.currentTodo}
        case 'SET_CURRENT_PAGE':
            return {...state, currentPage: action.currentPage}
        case 'GET_CURRENT_PAGE':
            return {...state, currentPage: state.currentPage}
        case 'SET_VIEWMODE':
            return {...state, viewMode: action.viewMode}
        case 'GET_VIEWMODE':
            return {...state, viewMode: state.viewMode}
        case 'OPEN_DELETE_MODAL':
            return {...state, deleteModalIsOpen: true, deleteTodoId: action.id}
        case 'CLOSE_DELETE_MODAL':
            return {...state, deleteModalIsOpen: false}
        case 'OPEN_EXPORT_MODAL':
            return {...state, exportModalIsOpen: true}
        case 'CLOSE_EXPORT_MODAL':
            return {...state, exportModalIsOpen: false}
        case 'OPEN_IMPORT_MODAL':
            return {...state, importModalIsOpen: true}
        case 'CLOSE_IMPORT_MODAL':
            return {...state, importModalIsOpen: false}
        default:
            return state
    }
}

export default todo

/**
 * ...args is rest parameter. this is parameter before assign.
 * exmaple 
 * func(...args){
 *  for (i in args) console.log(args[i])
 * }
 * func(1, 2, 3)
 * 
 * output => 1 2 3
 * 
 * 
 */