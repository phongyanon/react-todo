import { combineReducers } from 'redux'
import { connectRouter } from 'connected-react-router'
import { loadingBarReducer } from 'react-redux-loading-bar'

import auth from './auth'
import todo from './todo'

export default (history) => combineReducers({
    router: connectRouter(history),
    loadingBar: loadingBarReducer,
    auth,
    todo
})