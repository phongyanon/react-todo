import React from 'react'
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'connected-react-router';

import LoadingPage from './components/LoadingPage';
import { configStore, history } from './configStore';
import authAction from './actions/auth'

const AppRouter = React.lazy(() => new Promise(resolve => setTimeout(resolve, 800)).then(() => import('./router')));
const store = configStore();

authAction.checkSession().then((res) => {
	if(res.result === true){
		store.dispatch({type: 'SIGNIN'})
		store.dispatch({
			type: 'USERINFO', 
			userinfo: {
				username: res.session.user, 
				user_id: res.session.user_id
			}
		})
	}
}).catch((err) => {
	console.log('checkSession in App.js: ', err)
})


const App = () => {
	return (
		<Provider store={store}>
			<ConnectedRouter history={history}>
				<LoadingPage>
					<AppRouter />
				</LoadingPage>
			</ConnectedRouter>
		</Provider>
	)
}

export default App;
