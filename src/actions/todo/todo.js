import Api from '../../utils/axios'

export const ADD_TODO = 'ADD'
export const DELETE_TODO = 'DELETE'
export const UPDATE_TODO = 'UPDATE'
export const GET_TODO = 'GET'
export const GETALL_TODO = 'GETALL'
export const SETALL_TODO = 'SETALL'
export const SET_CURSOR = 'SET_CURSOR'
export const GET_CURSOR = 'GET_CURSOR'

export const SET_CURRENT_TODO = 'SET_CURRENT_TODO'
export const GET_CURRENT_TODO = 'GET_CURRENT_TODO'
export const SET_CURRENT_PAGE = 'SET_CURRENT_PAGE'
export const GET_CURRENT_PAGE = 'GET_CURRENT_PAGE'
export const SET_VIEWMODE = 'SET_VIEWMODE'
export const GET_VIEWMODE = 'GET_VIEWMODE'

export const OPEN_DELETE_MODAL = 'OPEN_DELETE_MODAL'
export const CLOSE_DELETE_MODAL = 'CLOSE_DELETE_MODAL'
export const OPEN_EXPORT_MODAL = 'OPEN_EXPORT_MODAL'
export const CLOSE_EXPORT_MODAL = 'CLOSE_EXPORT_MODAL'
export const OPEN_IMPORT_MODAL = 'OPEN_IMPORT_MODAL'
export const CLOSE_IMPORT_MODAL = 'CLOSE_IMPORT_MODAL'

export const mock_todos = [
    {id:1, name: "Java", date: "01-02-2019", description: "OOP programming language", status: "active"},
    {id:2, name: "C++", date: "02-02-2019", description: "C++ programming language", status: "active"},
    {id:3, name: "Golang", date: "03-02-2019", description: "Golang programming language", status: "active"},
    {id:4, name: "Flutter", date: "04-02-2019", description: "Flutter programming language", status: "active"},
    {id:5, name: "NodeJS", date: "05-02-2019", description: "NodeJS programming language", status: "completed"},
]

export const addTodo = (ctx) => {
    return async dispatch => {
        try {
            let data = { 
                name: ctx.name, 
                date: ctx.date, 
                description: ctx.description, 
                status: 'active' 
            }
            const result = await Api.post('/addTodo', data)
            // console.log('addTodo: ', result)
            data = result.data.result
            if((result.status === 200) && (data.status === 'success')){
                let todo = data.newTodo
                dispatch({type: ADD_TODO, todo: {
                    id: todo.id,
                    name: todo.name,
                    description: todo.description,
                    date: todo.date,
                    status: todo.status
                }})
                return {error: false, id: todo.id}
            } else {
                return {error: result}
            }
        } catch (err) {
            console.log('addTodo: ', err.toString())
            return {error: err.response}
        }
    }
}

export const importTodo = (todos) => {
    return async dispatch => {
        try {
            let data = { 
                todos: todos
            }
            const result = await Api.post('/importTodo', data)
            // console.log('addTodo: ', result)
            data = result.data.result
            if((result.status === 200) && (data.status === 'success')){
                for(let i in todos){
                    console.log(todos[i])
                    dispatch({type: ADD_TODO, todo: {
                        id: todos[i].id,
                        name: todos[i].name,
                        description: todos[i].description,
                        date: todos[i].date,
                        status: todos[i].status
                    }})
                }
                return({error: false})
            } else {
                return({error: result})
            }
        } catch (err) {
            console.log('addTodo: ', err.toString())
            return({error: err.response})
        }
    }
}

export const exportTodo = (ctx) => {
    return new Promise(async (resolve, reject) => {
        try {
            let result = await Api.get('/exportTodo', {
                params: {time_from: ctx.time_from, time_to: ctx.time_to}
            })
            console.log('getTodo: ', result)
            if(result.status === 200){
                resolve(result.data.result)
            }
        } catch (err) {
            reject(err.toString())
        }
    })
}

export const setCursor = (ctx) => {
    return dispatch => {
        dispatch({type: SET_CURSOR, currentTodo: ctx.currentTodo})
    }
}

export const getCursor = () => {
    return dispatch => {
        dispatch({type: GET_CURSOR})
    }
}

export const deleteTodo = (ctx) => {
    return async dispatch => {
        try {         
            let result = await Api.post('/deleteTodo', {id: ctx.id})
            let data = result.data.result
            if((result.status === 200) && (data.status === 'success')){
                dispatch({type: DELETE_TODO, todo: {id: ctx.id}})
                
                return {error: false}
            } else {
                return {error: result}
            }
        } catch (err) {
            return {error: err.toString()}            
        }
    }
}

export const updateTodo = (ctx) => {
    return async dispatch => {
        try {
            let data = {
                id: ctx.id,
                name: ctx.name,
                date: ctx.date,
                description: ctx.description,
                status: ctx.status
            }            
            let result = await Api.post('/updateTodo', data)

            data = result.data.result
            if((result.status === 200) && (data.status === 'success')){
                let updatedTodo = data.updatedTodo
                dispatch({type: UPDATE_TODO, id: updatedTodo.id, todo: updatedTodo})
                return {error: false}
            } else {
                return {error: result}
            }
        } catch (err) {
            return {error: err.toString()}            
        }
    }
}

export const updateStatusTodo = (ctx) => {
    return async dispatch => {
        try {
            let data = {
                id: ctx.id,
                status: ctx.status
            }            
            let result = await Api.post('/updateTodo', data)

            data = result.data.result
            if((result.status === 200) && (data.status === 'success')){
                let updatedTodo = data.updatedTodo
                dispatch({type: UPDATE_TODO, id: updatedTodo.id, todo: updatedTodo})
                return {error: false}
            } else {
                return {error: result}
            }
        } catch (err) {
            return {error: err.toString()}            
        }
    }
}

export const getTodo = (ctx) => {
    return dispatch => {
        dispatch({type: GET_TODO, id: ctx.id})
    }
}

export const getTodoApi = (ctx) => {
    return new Promise(async (resolve, reject) => {
        try {
            if(process.env.REACT_APP_DEV_MODE === 'standalone'){
                resolve({id: 99, name: "Mocha", date: "09-09-2019", description: "Coffee in the morning.", status: "active"})
            } else {
                let result = await Api.get('/getTodo', {
                    params: {id: ctx.id}
                })
                console.log('getTodo: ', result)
                if(result.status === 200){
                    resolve(result.data.result)
                }
            }
        } catch (err) {
            reject(err.toString())
        }
    })
}

export const getAllTodo = () => {
    return async dispatch => {
        dispatch({type: GETALL_TODO})
    }
}

export const setSearchTodo = (ctx) => {
    return async dispatch => {
        try {
            let result = await Api.get('/searchTodo', {
                params: {searchText: ctx.searchText}
            })
            if(result.status === 200 || result.data.result.status === 'success'){
                dispatch({type: SETALL_TODO, todos: result.data.result.todos})
            }
        } catch (err) {
            console.log(err)
            return {error: err.toString()}
        }
    }
}

export const setAllTodo = () => {
    return async dispatch => {
        try {
            if(process.env.REACT_APP_DEV_MODE === 'standalone'){
                dispatch({type: SETALL_TODO, todos: mock_todos})
            } else {
                let result = await Api.get('/getAllTodo')
                // console.log('getAllTodo: ', result)
                if(result.status === 200){
                    dispatch({type: SETALL_TODO, todos: result.data.result})
                }
            }
        } catch (err) {
            console.log(err)
            return {error: err.toString()}
        }
    }
}

export const getAllTodoApi = () => {
    return new Promise(async (resolve, reject) => {
        try {
            if(process.env.REACT_APP_DEV_MODE === 'standalone'){
                resolve(mock_todos)
            } else {
                let result = await Api.get('/getAllTodo')
                if(result.status === 200){
                    let todos = result.data.result
                    resolve(todos)
                }
            }
        } catch (err) {
            reject({error: err.toString()})
        }
    })
}

export const setStatusTodo = (ctx) => {
    return async dispatch => {
        try {
            let data = { 
                id: ctx.id,
                status: ctx.status
            }            
            let result = await Api.post('/updateTodo', data)

            data = result.data.result
            if((result.status === 200) && (data.status === 'success')){
                let todo = data.updatedTodo
                dispatch({type: UPDATE_TODO, todo: {
                    id: todo.id,
                    name: todo.name,
                    description: todo.description,
                    date: todo.date,
                    status: todo.status
                }})
                
                return {error: false, id: todo.id}
            } else {
                return {error: result}
            }
        } catch (err) {
            return {error: err.toString()}            
        }
    }
}

export const setCurrentTodo = (ctx) => {
    return dispatch => {
        dispatch({type: SET_CURRENT_TODO, currentTodo: ctx.currentTodo})
    }
}

export const getCurrentTodo = () => {
    return dispatch => {
        dispatch({type: GET_CURRENT_TODO})
    }
}

export const setCurrentPage = (ctx) => {
    return dispatch => {
        dispatch({type: SET_CURRENT_PAGE, currentPage: ctx.currentPage})
    }
}

export const getCurrentPage = () => {
    return dispatch => {
        dispatch({type: GET_CURRENT_PAGE})
    }
}

export const setViewMode = (ctx) => {
    return dispatch => {
        dispatch({type: SET_VIEWMODE, viewMode: ctx.viewMode})
    }
}

export const getViewMode = () => {
    return dispatch => {
        dispatch({type: GET_VIEWMODE})
    }
}

export const openDeleteModal = (todo_id) => {
    return dispatch => {
        dispatch({type: OPEN_DELETE_MODAL, id: todo_id})
    }
}

export const closeDeleteModal = () => {
    return dispatch => {
        dispatch({type: CLOSE_DELETE_MODAL})
    }
}

export const openExportModal = () => {
    return dispatch => {
        dispatch({type: OPEN_EXPORT_MODAL})
    }
}

export const closeExportModal = () => {
    return dispatch => {
        dispatch({type: CLOSE_EXPORT_MODAL})
    }
}

export const openImportModal = () => {
    return dispatch => {
        dispatch({type: OPEN_IMPORT_MODAL})
    }
}

export const closeImportModal = () => {
    return dispatch => {
        dispatch({type: CLOSE_IMPORT_MODAL})
    }
}