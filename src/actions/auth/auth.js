import Api from '../../utils/axios'
import Cookies from '../../utils/cookie'

export const SIGNIN = 'SIGNIN'
export const SIGNOUT = 'SIGNOUT'
export const USERINFO = 'USERINFO'
export const SIGNIN_FAILURE = 'SIGNIN_FAILURE'

export const checkSession = () => {
    return new Promise( async (resolve, reject) => {
        let session = Cookies.get('session')
        if(session === undefined) resolve({result: false})
        else {
            try {
                let result = await Api.get(
                    '/verify/session', {params: {session_id: session.session_id}}
                )
                result = result.data.result
                if(result === true){
                    resolve({result: true, session: session})
                } else {
                    resolve({result: false})
                }
            } catch (err) {
                console.log('checkSession: ', err.toString()) 
                reject({error: err.toString()})
    
            }
        }
    })
}

export const checkSignin = () => {
    return new Promise(async (resolve) => {
        try {
            let result = await Api.get('/isSignIn')
            resolve(result)
        } catch (err) {
            console.log('checkSignin: ', err.toString())
            resolve(false)
        }
    })
}

export const clearCookie = () =>
	new Promise(resolve => {
		Cookies.remove('session');
		resolve(true);
})

export const setCookie = (ctx) =>
    new Promise(resolve => {
        let data = { path: '/', expires: new Date(ctx.expires)}
        
        Cookies.set('session', {
            session_id: ctx.session_id,
            user_id: ctx.user_id,
            user: ctx.username
        }, data)

        resolve(true)
})

export const signUp = (username, password) => {
    return new Promise( async (resolve, reject) => {
        try {
            let body = {username: username, password: password, user_type_id: 2}
            const result = await Api.post('/signup', body)

            if (result.status === 200 && result.data.msg === 'success') {
                resolve({error: false})
            }
        } catch (err) {
            reject({error: err.response})
        }
    })
}

export const signIn = (username, password) => {
    return async dispatch => {
        signOut()
        try {
            let body = {username: username, password: password}
            const result = await Api.post(
                '/login', body
            )
            // console.log('signIn: ', result)
            await setCookie(result.data)
            dispatch({type: SIGNIN})
            dispatch({
                type: USERINFO, 
                userinfo: {
                    username: result.data.username, 
                    user_id: result.data.user_id
                }
            })
        } catch (err) {
            console.log('unauth: ', err.toString())
            return {error: err.response}
        }
    }
}

export const signOut = () => {
    return async dispatch => {
        try {
            let result = await Api.get(`/logout`);
            if (result.status === 200) {
                await clearCookie()
                dispatch({type: SIGNOUT})
            }
        } catch (err) {
            return {error: err.toString()}
        }
    }
}

export const forgetPassword = (ctx) => {
    return new Promise(async (resolve, reject) => {
        try {
            let data = { 
                username: ctx.username
            }
            let result = await Api.post('/forgetPassword', data)
            resolve(result.data.result)           
        } catch (err) {
            reject({error: err.toString()})
        }
    });
}

export const verifyResetPasswordToken = (ctx) => {
    return new Promise(async (resolve, reject) => {
        try {
            let result = await Api.get('/verifyResetPasswordToken', {params: {token: ctx.token}})
            resolve(result.data.result)           
        } catch (err) {
            reject({error: err.toString()})
        }
    });  
}

export const resetPassword = (ctx) => {
    return new Promise(async (resolve, reject) => {
        try {
            let data = { 
                password: ctx.password,
                token: ctx.token
            }
            let result = await Api.post('/resetPassword', data)
            resolve(result.data.result)           
        } catch (err) {
            reject({error: err.toString()})
        }
    });
}