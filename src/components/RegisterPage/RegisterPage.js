import React, { Component } from 'react'
import { Link, Redirect } from 'react-router-dom'

import authAction from '../../actions/auth'

class RegisterPage extends Component {
    constructor(props){
        super(props)
        this.state = {
            username: '',
            password: '',
            confirm: '',
            warning: '',
            submitted: false,
            signUpSuccess: false
        }

        this.handleChange = this.handleChange.bind(this)
        this.handleRegister = this.handleRegister.bind(this)
    }

    handleChange(e){
        const { name, value } = e.target
        this.setState({ [name]: value })
    }

    async handleRegister(e) {
        e.preventDefault()
        this.setState({ submitted: true })
        const {password, confirm} = this.state
        if(!password && !confirm) this.setState({warning: 'Password and confirm password is invalid.'})
        else{
            try {
                let {username, password} = this.state
                let res = await authAction.signUp(username, password)
                if(res.error === false) this.setState({signUpSuccess: true})
            } catch (err) {
                this.setState({warning: 'This username is already in used.'})
                console.log(err)
            }
        }    
    }


    render() {
        const {username, password, confirm, submitted, warning} = this.state
        if (this.state.signUpSuccess) return <Redirect to="/login" />   
        return (
            <div className="w3-card login-box">
                <h1>Sign up</h1>
                <form name="form" onSubmit={this.handleRegister}>
                <div className="textbox">
                    <i className="fa fa-user" aria-hidden="true"></i>
                    <input type="email" placeholder="Username" name="username" onChange={this.handleChange}/>
                </div>
                {submitted && !username && 
                        <div className="help-block">Username is required</div>
                    }
                <div className="textbox">
                    <i className="fa fa-lock" aria-hidden="true"></i>
                    <input type="password" placeholder="Password" name="password" onChange={this.handleChange}/>
                </div>
                {submitted && !password && 
                        <div className="help-block">Password is required</div>
                    }
                <div className="textbox">
                    <i className="fa fa-lock" aria-hidden="true"></i>
                    <input type="password" placeholder="Confirm Password" name="confirm" onChange={this.handleChange}/>
                </div>
                {submitted && !confirm && 
                        <div className="help-block">Confirm password is required</div>
                    }
                {submitted && warning &&
                        <div className="help-block">{warning}</div>
                    }
                <input className="btn-signup" type="submit" name="" value="Sign up"/>
                <p>Already have account? <Link to="/login" className="forgot-link">Sign in</Link></p>
                </form>
            </div>
        )
    }
}
export default RegisterPage