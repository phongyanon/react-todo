import React, { Component } from 'react'
import { Link, Redirect } from 'react-router-dom'
import { connect } from 'react-redux'

import authAction from '../../actions/auth'
import './LoginPage.sass'

class LoginPage extends Component {
    constructor(props){
        super(props)
        this.props.signOut()
        this.state = {
            username: '',
            password: '',
            submitted: false,
            warning: ''
        }

        this.handleChange = this.handleChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }

    handleChange(e) {
        const { name, value } = e.target
        this.setState({ [name]: value })
    }

    async handleSubmit(e) {
        e.preventDefault()

        this.setState({ submitted: true })
        const { username, password } = this.state
        const dispatch = this.props
        if (username && password){
            try {
                let res = await dispatch.signIn(username, password)
                if(res && res.error){
                    if(res.error.status === 401) this.setState({warning: 'Wrong username or password.'})
                    else this.setState({warning: 'Service is under maintainance.'})
                }   
            } catch (err) {
                console.log('login page error: ', err.toString())
            }
        }
    }

    render() {
        // const { loggingIn } = this.props
        const { username, password, submitted, warning } = this.state
        if (this.props.authenticate) return <Redirect to="/todo" />   
        return (
            <div className="w3-card login-box">
                <h1>Sign in</h1>
                <form name="form" onSubmit={this.handleSubmit}>
                    <div className="textbox">
                        <i className="fa fa-user" aria-hidden="true"></i>
                        <input type="text" placeholder="Username" name="username" onChange={this.handleChange}/>
                    </div>
                    {submitted && !username && 
                        <div className="help-block">Username is required</div>
                    }
                    <div className="textbox">
                        <i className="fa fa-lock" aria-hidden="true"></i>
                        <input type="password" placeholder="Password" name="password" value={password} onChange={this.handleChange}/>
                    </div>
                    {submitted && !password &&
                        <div className="help-block">Password is required.</div>
                    }
                    {submitted && warning &&
                        <div className="help-block">{warning}</div>
                    }
                    <input className="btn-login" type="submit" name="" value="Sign in"/>
                    <Link to="/forgetPassword" className="forgot-link">Forgot Password?</Link>
                    <Link to="/register" className="signup-link">Or Sign Up</Link>
                </form>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    // console.log(state)
    const { authenticate } = state.auth;
    return { authenticate };
}

const mapDispatchToProps = {
    signIn: authAction.signIn,
    signOut: authAction.signOut,
};

export default connect(mapStateToProps, mapDispatchToProps)(LoginPage);