import React from 'react'

import Layout from '../Layout'
import TodoForm from '../TodoForm'

const NewTodo = (props) => {
    return(
        <Layout showViewMode={false}>
            <TodoForm action="new" todo={false}></TodoForm>
        </Layout>
    )
    
}

export default NewTodo