import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import './HomePage.sass'
import programerImg from '../../images/programer.jpg'
import businessmanImg from '../../images/businessman.jpg'
import designerImg from '../../images/designer.jpg'
import wallImg from '../../images/wall.jpg'

class HomePage extends Component {
    constructor(props){
        super(props)
        this.toggleNav = this.toggleNav.bind(this)
    }

    toggleNav(e){
        let nav = document.getElementById("navDemo");
        if (nav.className.indexOf("w3-show") === -1) {
          nav.className += " w3-show";
        } else { 
          nav.className = nav.className.replace(" w3-show", "");
        }
    }

    render() {
        return (
            <div>

{/* <!-- Navbar --> */}
<div className="w3-top">
  <div className="w3-bar w3-card w3-left-align w3-large bg-white">
    <div className="w3-bar-item w3-button w3-hide-medium w3-hide-large w3-right w3-padding-large w3-hover-white w3-large bg-white" onClick={this.toggleNav} title="Toggle Navigation Menu"><i className="fa fa-bars"></i></div>
    <a href="#todo-header" className="w3-bar-item w3-padding-large todo-logo-pink">Todo</a>    
    <div className="todo-menu">
    {this.props.authenticated ? 
      <Link to="/todo" className="w3-hide-small w3-padding-large todo-sign-in w3-right">Console</Link>:
      <Link to="/login" className="w3-hide-small w3-padding-large todo-sign-in w3-right">sign in</Link>
    }
    <a href="#todo-contacts" className="w3-hide-small w3-padding-large todo-hover-pink w3-right">Contact</a>
    <a href="#todo-team" className="w3-hide-small w3-padding-large todo-hover-pink w3-right">Our Team</a>
    <a href="#todo-service" className="w3-hide-small w3-padding-large todo-hover-pink w3-right">Service</a>    
    <a href="#todo-header" className="w3-hide-small w3-padding-large todo-hover-pink w3-right">Home</a>
    </div>
  </div>

  {/* <!-- Navbar on small screens --> */}
  <div id="navDemo" className="w3-bar-block w3-hide w3-hide-large w3-hide-medium w3-large bg-light-pink">
    <a href="#todo-header" className="w3-bar-item w3-button w3-padding-large todo-hover-pink">Home</a>
    <a href="#todo-service" className="w3-bar-item w3-button w3-padding-large todo-hover-pink">Service</a>
    <a href="#todo-team" className="w3-bar-item w3-button w3-padding-large todo-hover-pink">Our Team</a>
    <a href="#todo-contacts" className="w3-bar-item w3-button w3-padding-large todo-hover-pink">Contact</a>
    {this.props.authenticated ? 
      <Link to="/todo" className="w3-bar-item w3-button w3-padding-large todo-hover-pink">Console</Link>:        
      <Link to="/login" className="w3-bar-item w3-button w3-padding-large todo-hover-pink">Sign in</Link>    
    }
  </div>
</div>

{/* <!-- Header --> */}
<header className="w3-container w3-center header-todo" id="todo-header">
  <h1 className="w3-margin w3-jumbo todo-wall-header">Welcome To Todo App</h1>
  <p className="w3-xlarge todo-wall-header">Application to plan your project or whatever you want.</p>
  <br/><br/>
  <div>
    <div className="todo-header-button">
      <Link className="todo-btn-start-pink" to="#">GET START <i className="fa fa-paper-plane" aria-hidden="true"></i></Link>    
    </div>
    <div className="todo-header-button">
      <Link className="todo-btn-start-yellow" to="#">LEARN MORE <i className="fa fa-book" aria-hidden="true"></i></Link>
    </div>
  </div>
</header>

{/* <!-- First Grid --> */}
<div className="w3-row-padding w3-padding-64 w3-container" id="todo-service">
  <div className="w3-content">
    <div className="w3-third w3-center todo-home-img">
      <img src={wallImg} alt=""/>
    </div>

    <div className="w3-twothird">
      <h1>Service</h1>
      <h5 className="w3-padding-32">
        Todo app is an application to plan whatever you want. Just like planing with post it paper. Support most of platform such as website and mobile. Let this app help you to plan your own life. 
      </h5>
    </div>
  </div>
</div>

{/* <!-- Second Grid --> */}
<div className="w3-row-padding w3-light-grey w3-padding-64 w3-container">
  <div className="w3-content">
    <div className="w3-twothird">
      <h1>Concept</h1>
      <h5 className="w3-padding-32">
        Post it paper is a good solution to plan a project or something else. It will better if you have some application that work like a post it paper but it can work anytime and anywhere. We beleive that all of goal will be success if we have a plan.
      </h5>
      </div>
    <div className="w3-third w3-center todo-home-img">
      <img src={wallImg} alt=""/>
    </div>
  </div>
</div>

{/* <!-- our team section --> */}
<div className="team-section" id="todo-team">
    <div className="inner-width">
        <h1>Meet Our Team</h1>
        <div className="pers">
            <div className="pe">
                <img src={programerImg} alt=""/>
                <div className="p-name">Phongyanon Y.</div>
                <div className="p-des">Programmer</div>
                <span className="p-short">Like to cooking with new ingrediant.</span>
            </div>

            <div className="pe">
                <img src={designerImg} alt=""/>
                <div className="p-name">Phongyanon Y.</div>
                <div className="p-des">Designer</div>
                <span className="p-short">Sport lover always go to gym.</span>
            </div>

            <div className="pe">
                <img src={businessmanImg} alt=""/>
                <div className="p-name">Phongyanon Y.</div>
                <div className="p-des">Business Analyst</div>
                <span className="p-short">Cat and chocolate lover. Extreme activity</span>
            </div>

        </div>
    </div>
</div>

<div className="w3-container w3-center w3-padding-64 todo-contacts" id="todo-contacts">
    <div className="w3-row todo-screen-size">
      <div className="section-title">Follow us on</div>
      <div className="border"></div>
        <div className="w3-xlarge">
            <div className="w3-xxlarge service-icon">
                <a href="https://www.facebook.com/phongyanon.yangen"><i className="fa fa-facebook-official w3-hover-opacity"></i></a>
                <a href="https://www.instagram.com/phongyanon/"><i className="fa fa-instagram w3-hover-opacity"></i></a>
                <a href="https://phongyanon.github.io/"><i className="fa fa-github w3-hover-opacity"></i></a>
                <a href="https://brain2com.blogspot.com/"><i className="fa fa-google-plus w3-hover-opacity"></i></a>
            </div>
        </div>
    </div>
    <div className="w3-row todo-phone-size">
      <div className="section-title">Follow us on</div>
      <div className="border"></div>
      <div className="w3-xlarge">
          <div className="service-icon">
              <a href="https://www.facebook.com/phongyanon.yangen"><i className="fa fa-facebook-official w3-hover-opacity"></i></a>
              <a href="https://www.instagram.com/phongyanon/"><i className="fa fa-instagram w3-hover-opacity"></i></a>
              <a href="https://phongyanon.github.io/"><i className="fa fa-github w3-hover-opacity"></i></a>
              <a href="https://brain2com.blogspot.com/"><i className="fa fa-google-plus w3-hover-opacity"></i></a>
          </div>
       </div>
    </div>
</div>

{/* <!-- Footer --> */}
<footer className="w3-container w3-center todo-footer">  
 <p>Powered by <a href="https://www.w3schools.com/w3css/default.asp" rel="noopener noreferrer" target="_blank">w3.css</a> and Designed by Phongyanon  Y.</p>
</footer>

            </div>
        )
    }
}

const mapStateToProps = (state) => ({
  authenticated: state.auth.authenticate
})
export default connect(mapStateToProps)(HomePage);