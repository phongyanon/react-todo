import React from 'react'
import Spinner from 'react-spinner-material'
import './LoadingPage.sass'

const LoadingPage = props => {
	return (
		<React.Suspense
			fallback={
				<div className="loading-content">
					<h1>Loading...</h1>
					<Spinner size={20} spinnerColor={"#e84393"} spinnerWidth={4} visible={true} />
				</div>
			}
		>
			{props.children}
		</React.Suspense>
	);
};

export default LoadingPage
