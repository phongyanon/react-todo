import React, { Component } from 'react'
import { connect } from 'react-redux'
import Spinner from 'react-spinner-material'

import Layout from '../Layout'
import TodoItem from '../TodoItem'
import TodoForm from '../TodoForm'
import todoAction from '../../actions/todo'
import { withRouter } from 'react-router-dom'
import './OverviewCard.sass'
import './OverviewPaper.sass'

class OverviewPage extends Component {
    constructor(props){
        super(props)
        this.state = {
            pageNum: localStorage.getItem('todo-pageNum') || 0,
            viewMode: localStorage.getItem('todo-viewMode') || 'list', // list, card, paper
            todos: [],
            loading: false,
            todoCategory: 'overview'
        }
        this.todoLength = this.props.todos.length || 0
        // console.log('OverviewPage: ', this.props)

        this.incresePageNum = this.incresePageNum.bind(this)
        this.decresePageNum = this.decresePageNum.bind(this)

        this.handleEditButton = this.handleEditButton.bind(this)
        this.handleActiveButton = this.handleActiveButton.bind(this)
        this.handleCompleteButton = this.handleCompleteButton.bind(this)
        this.openDeleteModal = this.openDeleteModal.bind(this)
    }

    async componentDidMount(){
        this.mounted = true // to prevent subscription memory leak
        try {
            let todoCategory = this.props.match.params
            if(todoCategory.hasOwnProperty('category') === false){
                todoCategory.category = 'overview'
            }
            if(this.mounted){
                this.props.setAllTodo()

                let {todos} = this.props
                if(todos.length < this.state.pageNum){
                    localStorage.setItem("todo-pageNum", 0)
                    this.setState({pageNum: 0})
                }
            }
        } catch (err) {
            console.log(err)
        }
    }

    componentWillUnmount(){
        this.mounted = false
        localStorage.setItem("todo-currentPage", "overview")
        this.props.setCurrentPage({currentPage: 'overview'})
    }

    handleEditButton(e){
        const { param } = e.target.dataset // get data from data-param in component      
        let path = `/todo/edit/${param}`
        this.props.history.push(path)
    }

    sleep = (milliseconds) => { // hold time for see spinner
        return new Promise((resolve) => setTimeout(resolve, milliseconds));
    };


    async handleActiveButton(e){
        try {
            this.setState({loading: true})
            const { param } = e.target.dataset
            await this.sleep(2000)
            await this.props.updateStatusTodo({id: param, status: 'active'})
            this.setState({loading: false})
        } catch (err) {
            console.log(err)
        }
    }

    async handleCompleteButton(e){
        try {
            this.setState({loading: true})
            const { param } = e.target.dataset
            await this.sleep(2000)
            await this.props.updateStatusTodo({id: param, status: 'completed'})
            this.setState({loading: false})
        } catch (err) {
            console.log(err)
        }
    }

    incresePageNum(){
        let todo_length = this.todoLength
        this.setState({pageNum: (this.state.pageNum + 1) % todo_length})
    }

    decresePageNum(){
        let todo_length = this.todoLength
        let newPageNum = this.state.pageNum - 1
        if(newPageNum < 0) this.setState({pageNum: todo_length - 1})
        else this.setState({pageNum: newPageNum})
    }

    handleViewMode(){
        // let todos = this.state.todos
        let { viewMode, currentPage, todos } = this.props

        if(currentPage === 'active'){
            todos = todos.filter((item) => {
                return item.status === 'active'
            })
        } else if(currentPage === 'completed'){
            todos = todos.filter((item) => {
                return item.status === 'completed'
            })
        } else if(currentPage === 'new'){
            return <TodoForm action="new" todo=""></TodoForm>
        }

        let todosItem = []
        this.todoLength = todos.length
        viewMode = this.props.viewMode

        if((viewMode === 'list') || (viewMode === 'card')){
            for(let i in todos){
                todosItem.push(
                    <TodoItem 
                        viewMode={viewMode} 
                        todo={todos[i]} 
                        key={`todo-${i}`}
                    ></TodoItem>
                )
            }
            if(viewMode === 'list') return <div className="space-card">{todosItem}</div>
            else return <div className="space-card-rect">{todosItem}</div>
        } else if(viewMode === 'paper'){
            if(todos.length > 0){
                let pageNum = this.state.pageNum
                if(todos.length <= pageNum){
                    localStorage.setItem("todo-pageNum", 0)
                    pageNum = 0
                }
                let todo = todos[pageNum]
                let todoDate = todo.todo_date
                return(
                <>
                    <div className="space-view">
                        <div className={todo.status === 'active'?
                            "card card-active":"card card-complete"}>
                            <div className="card-action">
                                {todo.status === 'active'?
                                    <button className="complete-action" data-param={todo.id} onClick={this.handleCompleteButton}>Complete</button>:
                                    <button className="active-action" data-param={todo.id} onClick={this.handleActiveButton}>Active</button>
                                }
                                <button className="edit-action" data-param={todo.id} onClick={this.handleEditButton}>Edit</button>
                                <button className="del-action" data-param={todo.id} onClick={this.openDeleteModal}>Delete</button>
                                { this.state.loading && <Spinner size={25} spinnerColor={"#e84393"} spinnerWidth={4} visible={true} />}
                            </div>
                            <h3>  
                                {todo.name}                    
                            </h3>
                            <p>{todoDate}</p>
                            <span>
                            {todo.description}
                            </span>

                            <div className="changePage">
                                <div>
                                <button className="changePage-button" onClick={this.decresePageNum}>
                                    <i className="fa fa-angle-left" aria-hidden="true"></i>
                                </button>
                                <span>  {(parseInt(pageNum, 10) + 1)}/{todos.length}  </span>
                                <button className="changePage-button" onClick={this.incresePageNum}>
                                    <i className="fa fa-angle-right" aria-hidden="true"></i>
                                </button>
                                </div>
                            </div>

                        </div>
                    </div>
                </>
                )
            }
        } else {
            return(<></>)
        }
    }

    openDeleteModal(e){
        const { param } = e.target.dataset
        this.props.openDeleteModal(param)
    }

    render() {
        console.log('render: ', this.props.match.params, this.state.todos)
        
        const itemComponent = this.handleViewMode()
        return (
            <Layout showViewMode={true}>
                {itemComponent}
            </Layout>
        )
    }
}

const mapStateToProps = (state) => {
    // console.log('mapStateToProps: ', state)
    const { userinfo } = state.auth
    const { viewMode, currentPage, todos } = state.todo
    return { userinfo, viewMode, currentPage, todos }
}

const mapDispatchToProps = {
    setCurrentPage: todoAction.setCurrentPage,
    setAllTodo: todoAction.setAllTodo,
    updateStatusTodo: todoAction.updateStatusTodo,
    openDeleteModal: todoAction.openDeleteModal
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(OverviewPage))

