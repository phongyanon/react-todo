import React, { Component } from 'react'
// import { Link } from 'react-router-dom'
import authAction from '../../actions/auth'
import Spinner from 'react-spinner-material'

import '../LoadingPage/LoadingPage.sass'

class ForgetPasswordPage extends Component {
    constructor(props){
        super(props)
        this.state = {
            email: '',
            message: '',
            loading: false,
            submitted: false
        }

        this.handleChange = this.handleChange.bind(this)
        this.handleResetPassword = this.handleResetPassword.bind(this)
    }

    handleChange(e){
        const { name, value } = e.target
        this.setState({ [name]: value })
    }

    async handleResetPassword(e) {
        e.preventDefault()
        if(this.state.email){
            this.setState({ submitted: true, loading: true})
            // console.log('Reset Password: ', this.state)
            try {
                let result = await authAction.forgetPassword({username: this.state.email})
                console.log(result)
                if(result.msg === 'success') this.setState({
                    message: 'please go to your email to reset password.',
                    loading: false
                })
            } catch (err) {
                console.log(err);
            }   
        } else this.setState({ submitted: true})
    }


    render() {
        const {email, submitted} = this.state
        return (
            <div className="w3-card login-box">
                <h2>Reset Password</h2>
                <form name="form" onSubmit={this.handleResetPassword}>
                    <div className="textbox">
                        <i className="fa fa-paper-plane" aria-hidden="true"></i>
                        <input type="email" placeholder="Email Address" name="email" onChange={this.handleChange}/>
                    </div>
                    { this.state.loading && <Spinner size={20} spinnerColor={"#e84393"} spinnerWidth={4} visible={true} />}
                    <div className="success-message">{this.state.message}</div>
                    {submitted && !email && 
                        <div className="help-block">Email is required</div>
                    }
                    <input className="btn-login" type="submit" name="" value="Reset Password"/>
                </form>
            </div>
        )
    }
}
export default ForgetPasswordPage