import React, { Component } from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom';
import Spinner from 'react-spinner-material'

import todoAction from '../../actions/todo'
import Layout from '../Layout'
import './TodoPaper.sass'

class TodoPaper extends Component {
    constructor(props){
        super(props)
        this.state = {
            todo: false,
            loading: false
        }
        this.handleEditButton = this.handleEditButton.bind(this)
        this.handleActiveButton = this.handleActiveButton.bind(this)
        this.handleCompleteButton = this.handleCompleteButton.bind(this)
        this.openDeleteModal = this.openDeleteModal.bind(this)
    }

    async componentDidMount(){
        this.mount = true
        try {
            let params = this.props.match.params
            if(params.hasOwnProperty('id') === false){
                this.setState({todo: false})
            } else if(this.mount) {
                let todo = await todoAction.getTodoApi({id: params.id})
                this.setState({
                    todo: todo
                })
            }
        } catch (err) {
            console.log(err)
        }
    }

    componentWillUnmount(){
        this.mount = false
    }

    openDeleteModal(e){
        const { param } = e.target.dataset
        this.props.openDeleteModal(param)
    }

    handleEditButton(e){
        const { param } = e.target.dataset // get data from data-param in component      
        let path = `/todo/edit/${param}`
        this.props.history.push(path)
    }

    async handleActiveButton(e){
        try {
            this.setState({loading: true})
            const { param } = e.target.dataset
            let result = await this.props.updateStatusTodo({id: param, status: 'active'})
            if(result.error === false){
                let todo = this.state.todo
                todo.status = 'active'
                this.setState({todo: todo, loading: false})
            }
        } catch (err) {
            console.log(err)
        }
    }

    async handleCompleteButton(e){
        try {
            this.setState({loading: true})
            const { param } = e.target.dataset
            let result = await this.props.updateStatusTodo({id: param, status: 'completed'})
            if(result.error === false){
                let todo = this.state.todo
                todo.status = 'completed'
                this.setState({todo: todo, loading: false})
            }
        } catch (err) {
            console.log(err)
        }
    }

    render(){
        let todo = this.state.todo
        let itemTodo

        if(todo !== false){
            if(todo.id === undefined){
                itemTodo = (
                    <div className="loading-content">
                        <h3>No Content</h3>
                    </div>
                )
            } else {
                let todoDate = todo.todo_date
                itemTodo = (
                    <div className="space-view"> 
                        <div className={todo.status === 'active'?
                            "card card-active":"card card-complete"}>
                            <div className="card-action">
                                {todo.status === 'active'?
                                    <button className="complete-action" data-param={todo.id} onClick={this.handleCompleteButton}>Complete</button>:
                                    <button className="active-action" data-param={todo.id} onClick={this.handleActiveButton}>Active</button>
                                }
                                <button className="edit-action" data-param={todo.id} onClick={this.handleEditButton}>Edit</button>
                                <button className="del-action" data-param={todo.id} onClick={this.openDeleteModal}>Delete</button>
                                { this.state.loading && <Spinner size={25} spinnerColor={"#e84393"} spinnerWidth={4} visible={true} />}                                
                            </div>
                            <h3>  
                                {todo.name}                    
                            </h3>
                            <p>{todoDate}</p>
                            <span>
                               {todo.description}
                            </span>
                        </div>
                    </div>
                )
            }
        } else {
            itemTodo = (
				<div className="loading-content">
					<h3>Loading...</h3>
				</div>
            )
        }
        return(
            <Layout showViewMode={false}>
                {itemTodo}
            </Layout>
        )
    }
}

const mapDispatchToProps = {
    updateStatusTodo: todoAction.updateStatusTodo,
    openDeleteModal: todoAction.openDeleteModal
}

export default withRouter(connect(null, mapDispatchToProps)(TodoPaper))