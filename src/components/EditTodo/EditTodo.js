import React, { Component } from 'react'

import Layout from '../Layout'
import todoAction from '../../actions/todo'
import TodoForm from '../TodoForm'

class EditTodo extends Component {
    constructor(props){
        super(props)
        this.state = {
            id: '',
            name: '',
            todo_date: '',
            description: ''
        }
    }

    async componentWillMount() {
        try {
            let result = await todoAction.getTodoApi({id: this.props.match.params.id})
            this.setState({
                id: result.id,
                name: result.name,
                todo_date: result.todo_date,
                description: result.description
            })
        } catch (err) {
            console.log('EditTodo: ', err.toString())
        }
    }

    render(){
        let {id, name, description, todo_date} = this.state
        let todo = false
        if(id !== "") todo = { id: id, name: name, description: description, todo_date: todo_date }
        console.log('render: ', this.props, todo)
        return(
            <Layout showViewMode={false}>
                <TodoForm action="update" todo={todo}></TodoForm>
            </Layout>
        )
    }
}

export default EditTodo