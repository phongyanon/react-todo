import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Redirect, Link } from 'react-router-dom'

import ExportModal from '../TodoModal/ExportModal'
import ImportModal from '../TodoModal/ImportModal'
import DeleteModal from '../TodoModal/DeleteModal'
import todoAction from '../../actions/todo'
import './Layout.sass'

class Layout extends Component {
    constructor(props){
        super(props)
        this.state = {
            redirect: false,
            target: '/',
            // currentPage: 'overview', // overview, active, complete, new
            // viewMode: localStorage.getItem('todo-viewMode') || 'list', // list, card, paper
            searchText: '',
            currentTodo: false,
        }
        this.menuStatus = 'off'
        this.modalIsOpen = false

        this.toggleMenu = this.toggleMenu.bind(this)
        this.handleOverviewButton = this.handleOverviewButton.bind(this)
        this.handleActiveButton = this.handleActiveButton.bind(this)
        this.handleCompleteButton = this.handleCompleteButton.bind(this)
        this.handleImportButton = this.handleImportButton.bind(this)
        this.handleExportButton = this.handleExportButton.bind(this)

        this.renderRedirect =  this.renderRedirect.bind(this)
        this.handleLogoutButton = this.handleLogoutButton.bind(this)
        this.handleSearchButton = this.handleSearchButton.bind(this)
        this.handleChange = this.handleChange.bind(this)

        this.handleViewList = this.handleViewList.bind(this)
        this.handleViewTh = this.handleViewTh.bind(this)
        this.handleViewCard = this.handleViewCard.bind(this)
    }

    toggleMenu = (e) => {
        let menu = document.getElementById("toggle-menu");

        if(this.menuStatus === 'on'){
          this.menuStatus = 'off';
          menu.style.height = '0';
        } else {
          this.menuStatus = 'on';
          menu.style.height = '200px';
          document.body.scrollTop = 0; // For Safari
          document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
        }
    }

    handleOverviewButton(e){
        localStorage.setItem("todo-currentPage", "overview")
        this.props.setCurrentPage({currentPage: 'overview'})
    }
    handleActiveButton(e){
        localStorage.setItem("todo-currentPage", "active")
        this.props.setCurrentPage({currentPage: 'active'})
    }
    handleCompleteButton(e){
        localStorage.setItem("todo-currentPage", "completed")
        this.props.setCurrentPage({currentPage: 'completed'})
    }

    handleChange(e){
        const { name, value } = e.target
        this.setState({ [name]: value })
    }

    handleSearchButton(e){
        this.props.setSearchTodo({searchText: this.state.searchText})
    }

    handleImportButton(e){
        this.props.openImportModal()
    }
    
    handleExportButton(e){
        this.props.openExportModal()
    }

    setRedirect(){
        this.setState({
          redirect: true
        })
    }

    renderRedirect(){
        if (this.state.redirect) {
          return <Redirect to={this.state.target} />
        }
    }

    handleLogoutButton(){
        this.setState({
            redirect: true,
            target: "/login"
          })
    }

    handleViewList(){
        localStorage.setItem("todo-viewMode", "list")
        this.props.setViewMode({viewMode: 'list'})
    }

    handleViewTh(){
        localStorage.setItem("todo-viewMode", "card")
        this.props.setViewMode({viewMode: 'card'})
    }
    
    handleViewCard(){
        localStorage.setItem("todo-viewMode", "paper")
        this.props.setViewMode({viewMode: 'paper'})
    }

    render() {
        const user = this.props.userinfo
        let username
        if(user !== undefined) username = user.username

        return (
        <>
            {this.renderRedirect()}
            <nav>
                <h3>Todo</h3>
                <div className="hamburger">
                    <i className="fa fa-align-justify" aria-hidden="true" onClick={this.toggleMenu}></i>
                </div>
                <div className="search-bar">
                    <input type="text" name="searchText" placeholder="Search" onChange={this.handleChange}/>
                    <button onClick={this.handleSearchButton}><i className="fa fa-search" aria-hidden="true"></i></button>
                </div>
            </nav>
    
            <Link to="/todo/new"><div className="float-btn"><i className="fa fa-plus new-float-btn"></i></div></Link>
    
        <div className="main-space">
            <div className="toggle-menu" id="toggle-menu">
                <div className="username">User {username}</div>
                <Link to="/"><button id="home-btn">Home</button></Link>
                <Link to="/todo"><button id="active-btn">Overview</button></Link>
                <Link to="/todo/active"><button id="active-btn">Active</button></Link>
                <Link to="/todo/completed"><button id="active-btn">Completed</button></Link>
                <Link to="#"><button id="import-btn" onClick={this.handleImportButton}>Import</button></Link>
                <Link to="#"><button id="export-btn" onClick={this.handleExportButton}>Export</button></Link>   
                <button id="logout-btn" onClick={this.handleLogoutButton}>Logout</button>
            </div>
            <div className="main-menu">
                <div className="username">User {username}</div>
                <Link to="/"><button id="home-btn">Home</button></Link>
                <Link to="/todo" onClick={this.handleOverviewButton}><button id="active-btn">Overview</button></Link>
                <Link to="/todo/active" onClick={this.handleActiveButton}><button id="active-btn">Active</button></Link>
                <Link to="/todo/completed" onClick={this.handleCompleteButton}><button id="active-btn">Completed</button></Link>
                <Link to="#"><button id="import-btn" onClick={this.handleImportButton}>Import</button></Link>
                <Link to="#"><button id="export-btn" onClick={this.handleExportButton}>Export</button></Link>                
                <button className="logout-btn" id="logout-btn" onClick={this.handleLogoutButton}>Logout</button>    
            </div>
            <div className="space">
                <div className="space-menu">
                    <div className="action-btn">
                    <Link to="/todo/new"><button className="new-action">New</button></Link>
                    </div>
                    {(this.props.showViewMode)?
                        <div className="view-icon">
                            <i className="fa fa-list-ul" onClick={this.handleViewList} aria-hidden="true"></i>
                            <i className="fa fa-th" onClick={this.handleViewTh} aria-hidden="true"></i>
                            <i className="fa fa-credit-card" onClick={this.handleViewCard} aria-hidden="true"></i>
                        </div>
                    :<></>}
                </div>

                {this.props.children}
                <DeleteModal/>
                <ExportModal/>
                <ImportModal/>
            </div>
        </div>
        </>
        )
    }
}

const mapStateToProps = (state) => {
    const { userinfo } = state.auth
    const { todos } = state.todo
    return { userinfo, todos }
}

const mapDispatchToProps = {
    setCurrentPage: todoAction.setCurrentPage,
    setViewMode: todoAction.setViewMode,
    openExportModal: todoAction.openExportModal,
    openImportModal: todoAction.openImportModal,
    setSearchTodo: todoAction.setSearchTodo
}

export default connect(mapStateToProps, mapDispatchToProps)(Layout)