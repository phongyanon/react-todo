import React from 'react'
import { connect } from 'react-redux'
import { Route, Redirect } from 'react-router-dom'

const PrivateRoute = ({ authenticated, component: Component, ...rest }) => {
    // console.log('private: ', authenticated)

    if(process.env.REACT_APP_DEV_MODE === 'standalone'){
        return (
            <Route {...rest} render={props =>
                <Component {...props} />
                }
            />
        );
    } else{
        return (
            <Route {...rest} render={props =>
                    authenticated 
                        ? <Component {...props} />
                        : <Redirect to={{ pathname: '/login', state: { from: props.location } }}/>
                }
            />
        );
    }

}

const mapStateToProps = (state) => ({
    authenticated: state.auth.authenticate
})
export default connect(mapStateToProps)(PrivateRoute);
