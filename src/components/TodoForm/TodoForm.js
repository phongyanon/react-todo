import React, { Component } from 'react'
import { connect } from 'react-redux'

import todoAction from '../../actions/todo'
import {  Redirect } from 'react-router-dom'
import './TodoForm.sass'

class TodoForm extends Component {
    constructor(props){
        super(props)
        this.state = {
            name: '',
            date: '',
            description: '', 
            submitted: false,
            target: '/todo'
        }
        this.handleAddSubmit = this.handleAddSubmit.bind(this)
        this.handleUpdateSubmit = this.handleUpdateSubmit.bind(this)
        this.handleChange = this.handleChange.bind(this)
        this.handleCancel =  this.handleCancel.bind(this)
        this.renderRedirect =  this.renderRedirect.bind(this)
    }

    componentWillReceiveProps(nextProps){
        let { action, todo } = nextProps
        if ((action === 'update') && (todo !== false)){
            this.setState({name: todo.name, date: todo.date, description: todo.description})
        }
    }

    handleChange(e) {
        const { name, value } = e.target
        this.setState({ [name]: value })
    }

    sleep = (milliseconds) => { // hold time for see spinner
        return new Promise((resolve) => setTimeout(resolve, milliseconds));
    };

    async handleAddSubmit(e){
        e.preventDefault()

        const { name, date, description } = this.state
        const dispatch = this.props

        try {
            let result = await dispatch.addTodo({ name: name, date: date, description: description })
            if(result.error === false){
                await dispatch.getTodo({id: result.id})
                this.setState({ submitted: true, target: `/todo/view/${this.props.currentTodo.id}`})
            }
        } catch (err) {
            // handle add todo failed
        }
    }

    async handleUpdateSubmit(e){
        e.preventDefault()

        const { name, date, description } = this.state
        const dispatch = this.props

        try {
            let result = await dispatch.updateTodo({
                id: this.props.todo.id,
                name: name, 
                date: date, 
                description: description 
            })
            if(result.error === false){
                this.setState({ submitted: true, target: `/todo/view/${this.props.todo.id}`})
            }
        } catch (err) {
            // handle update todo failed by show modal update failed
        }
    }

    handleCancel(){
        this.setState({ submitted: true})
    }

    renderRedirect(){
        if (this.state.submitted) {
            if ((this.props.action === 'update') && (this.props.todo !== false)){
                return <Redirect to={`/todo/view/${this.props.todo.id}`} />
            } else {
                return <Redirect to={this.state.target} />
            }
        }
    }

    render(){
        let inputItem = (<></>)

        if(this.props.action === 'new'){
            inputItem = (
                <form onSubmit={this.handleAddSubmit} method="post">
                    <h4>Name:</h4>
                    <input type="text" name="name" onChange={this.handleChange} placeholder="title name" defaultValue="" required/>

                    <h4>Date:</h4>
                    <input className="datetime-input" name="date" onChange={this.handleChange} type="date" defaultValue="" required/>

                    <h4>Description:</h4>
                    <textarea cols="30" rows="10" name="description" onChange={this.handleChange} placeholder="description" defaultValue="" required></textarea>

                    <div className="new-todo-action">
                        <button className="submit-action" type="submit">Create</button>
                        <button className="cancel-action" onClick={this.handleCancel}>Cancel</button>
                    </div>
                </form>
            )
        }
        else if ((this.props.action === 'update') && (this.props.todo !== false)){
            let todo = this.props.todo
            let todoDate = todo.todo_date.split('/')
            let todoDefault = `${todoDate[2]}-${todoDate[1]}-${todoDate[0]}`
            inputItem = (
                <form onSubmit={this.handleUpdateSubmit} method="post">
                    <h4>Name:</h4>
                    <input type="text" name="name" onChange={this.handleChange} placeholder="title name" required defaultValue={todo.name}/>
        
                    <h4>Date:</h4>
                    <input className="datetime-input" name="date" onChange={this.handleChange} type="date" required defaultValue={todoDefault}/>
        
                    <h4>Description:</h4>
                    <textarea cols="30" rows="10" name="description" onChange={this.handleChange} placeholder="description" defaultValue={todo.description} required></textarea>

                    <div className="new-todo-action">
                        <button className="submit-action" type="submit">Update</button>
                        <button className="cancel-action" onClick={this.handleCancel}>Cancel</button>
                    </div>
                </form>
            )
        }
        return(
            <div className="space-view">
                <div className="card">
                    {this.renderRedirect()}
                    {inputItem}
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    const { todos, currentTodo } = state.todo
    return { todos, currentTodo }
}

const mapDispatchToProps = {
    addTodo: todoAction.addTodo,
    getTodo: todoAction.getTodo,
    updateTodo: todoAction.updateTodo
};

export default connect(mapStateToProps, mapDispatchToProps)(TodoForm)