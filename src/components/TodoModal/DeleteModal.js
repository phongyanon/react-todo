import React, { Component } from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'

import todoAction from '../../actions/todo'
import './DeleteModal.sass'

class DeleteModal extends Component {
    constructor(props){
        super(props)
        this.state = {
            modalIsOpen: false
        }

        this.deleteTodo = this.deleteTodo.bind(this);
        this.closeModal = this.closeModal.bind(this);
        
    }

    deleteTodo() {
        this.props.deleteTodo({id: this.props.deleteTodoId})
        this.props.closeDeleteModal()
        let path = `/todo`
        this.props.history.push(path)
      }
     
    closeModal() {
        this.props.closeDeleteModal()
    }

    render() {
        if(this.props.deleteModalIsOpen){
            return(
                <div className="w3-modal delete-todo-modal">
                    <div className="modal-content">
                        <span className="close" onClick={this.closeModal}>&times;</span>
                        <h4>Are you sure to delete this todo?</h4>
                        <div className="delete-action-btn">
                            <button className="delete-action" onClick={this.deleteTodo}>Delete</button>
                            <button className="cancel-action" onClick={this.closeModal}>Cancel</button>
                        </div>
                    </div>
                </div>
                )
        } else {
            return(<></>)
        }
    }
}

const mapStateToProps = (state) => {
    const { deleteModalIsOpen, deleteTodoId } = state.todo
    return { deleteModalIsOpen, deleteTodoId }
}

const mapDispatchToProps = {
    closeDeleteModal: todoAction.closeDeleteModal,
    deleteTodo: todoAction.deleteTodo
}


export default withRouter(connect(mapStateToProps, mapDispatchToProps)(DeleteModal))