import React, { Component } from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'

import todoAction from '../../actions/todo'
import './ExportModal.sass'

class ExportModal extends Component {
    constructor(props){
        super(props)
        this.state = {
            modalIsOpen: false,
            time_from: null,
            time_to: null,
            warning: ''
        }

        this.exportTodo = this.exportTodo.bind(this)
        this.handleChange = this.handleChange.bind(this)
        this.closeModal = this.closeModal.bind(this)
    }

    componentWillReceiveProps(nextProps){
        this.setState({warning: ''})
    }

    handleChange(e) {
        const { name, value } = e.target
        this.setState({ [name]: value })
    }

    async exportTodo(e) {
        e.preventDefault()

        let { time_from, time_to } = this.state
        if(time_from >= time_to){
            this.setState({warning: "Time from must less than time to."})
        } else if(time_to === null || time_from === null){
            this.setState({warning: "Time from and time to must no be null."})
        } else {
            try {
                let result = await todoAction.exportTodo({time_from: time_from, time_to: time_to})

                let csv = 'id,name,description,date,status\n';
                result.todos.forEach(function(row) {
                        csv += `${row.id},"${row.name}","${row.description}",${row.date},${row.status}\n`
                })
             
                var hiddenElement = document.createElement('a');
                hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(csv);
                hiddenElement.target = '_blank';
                hiddenElement.download = 'todoList.csv';
                hiddenElement.click();

                this.props.closeExportModal()
                let path = `/todo`
                this.props.history.push(path)   
            } catch (err) {
                console.log(err)
            }
        }
      }
     
    closeModal(e) {
        this.props.closeExportModal()
    }

    render() {
        let { warning } = this.state
        if(this.props.exportModalIsOpen){
            return(
            <div className="w3-modal export-todo-modal">
                <div className="modal-content">
                    <span className="close" onClick={this.closeModal}>&times;</span>
                    <h4>Export CSV File</h4>
                    <form name="form" onSubmit={this.exportTodo}>
                        <p>Time from:</p>
                        <input type="date" name="time_from" onChange={this.handleChange}/>
                        <p>Time to:</p>
                        <input type="date" name="time_to" onChange={this.handleChange}/>
                        {warning &&
                            <div className="help-block">{warning}</div>
                        }
                        <div className="download-action-btn">
                            <button className="download-action" type="submit">Download</button>
                            <button className="cancel-action" onClick={this.closeModal}>Cancel</button>
                        </div>
                    </form>
                </div>
            </div>
                )
        } else {
            return(<></>)
        }
    }
}

const mapStateToProps = (state) => {
    const { exportModalIsOpen } = state.todo
    return { exportModalIsOpen }
}

const mapDispatchToProps = {
    closeExportModal: todoAction.closeExportModal
}


export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ExportModal))