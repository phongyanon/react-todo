import React, { Component } from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import CSVReader from "react-csv-reader"

import todoAction from '../../actions/todo'
import './ImportModal.sass'

class ImportModal extends Component {
    constructor(props){
        super(props)
        this.state = {
            modalIsOpen: false,
            importTodos: null,
            warning: ''
        }

        this.importTodo = this.importTodo.bind(this)
        this.handleChange = this.handleChange.bind(this)
        this.closeModal = this.closeModal.bind(this)
    }

    componentWillReceiveProps(nextProps){
        this.setState({warning: ''})
    }

    handleChange(data) {
        try {
            if(data[0][0] === 'name' && data[0][1] === 'description' && data[0][2] === 'date'){
                this.setState({ importTodos: data })
            }
            else this.setState({warning: 'please import .csv file.'})
        } catch (err) {
            this.setState({warning: 'please choose some file.'})
        }
    }

    async importTodo(e) {
        e.preventDefault()
        let {importTodos} = this.state
        try {
            if(importTodos[0][0] === 'name' && importTodos[0][1] === 'description' && importTodos[0][2] === 'date'){
                importTodos.shift()
                let todos = []
                for(let i in importTodos){
                    todos.push({
                        name: importTodos[i][0],
                        description: importTodos[i][1],
                        date: importTodos[i][2],
                        status: 'active'
                    })
                }
                try {
                    await this.props.importTodo(todos)
                } catch (err) {
                    console.log(err)
                }
                this.props.closeImportModal()
                let path = `/todo`
                this.props.history.push(path)
            }
            else this.setState({warning: 'please import .csv file.'})
        } catch (err) {
            this.setState({warning: 'please choose some .csv file.'})
        }
      }
     
    closeModal(e) {
        this.props.closeImportModal()
    }

    render() {
        let { warning } = this.state
        if(this.props.importModalIsOpen){
            return(
                <div className="w3-modal import-todo-modal">
                    <div className="modal-content">
                        <span className="close" onClick={this.closeModal}>&times;</span>
                        <h4>Import CSV File</h4>
                        <div className="upload-file-form">
                            <CSVReader
                                cssClass="upload-file"
                                onFileLoaded={this.handleChange}
                            />
                            { warning && 
                                 <div className="help-block">{warning}</div>
                            }
                            <div className="upload-action-btn">
                                <button className="upload-action" onClick={this.importTodo} type="submit">Upload</button>
                                <button className="cancel-action" onClick={this.closeModal}>Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
                )
        } else {
            return(<></>)
        }
    }
}

const mapStateToProps = (state) => {
    const { importModalIsOpen } = state.todo
    return { importModalIsOpen }
}

const mapDispatchToProps = {
    closeImportModal: todoAction.closeImportModal,
    importTodo: todoAction.importTodo,
    setAllTodo: todoAction.setAllTodo,
}


export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ImportModal))