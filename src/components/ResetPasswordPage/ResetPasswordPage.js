import React, { Component } from 'react'
import { Redirect } from 'react-router-dom'
import authAction from '../../actions/auth'
import Spinner from 'react-spinner-material'

import '../LoadingPage/LoadingPage.sass'

class ResetPasswordPage extends Component {
    constructor(props){
        super(props)
        this.state = {
            password: '',
            message: '',
            token: '',
            loading: false,
            success: false,
            submitted: false
        }

        this.handleChange = this.handleChange.bind(this)
        this.handleResetPassword = this.handleResetPassword.bind(this)
    }

    async componentWillMount() {
        let token = this.props.match.params.token;
        console.log(token)
        try {
            let result = await authAction.verifyResetPasswordToken({token: token})
            if(result.status === 'success' && result.msg) this.setState({token: token})
            else this.setState({token: false})
        } catch (err) {
            this.setState({token: false})
            console.log(err)
        }
    }

    handleChange(e){
        const { name, value } = e.target
        this.setState({ [name]: value })
    }

    async handleResetPassword(e) {
        e.preventDefault()
        if(this.state.password){
            this.setState({ submitted: true, loading: true})
            try {
                let result = await authAction.resetPassword({
                    password: this.state.password,
                    token: this.state.token
                })
                // console.log(result)
                if(result.status === 'success') {
                    this.setState({
                        success: true,
                        loading: false
                    })
                }
                else {
                    this.setState({
                        message: 'Can not reset your password.',
                        loading: false
                    })
                }
            } catch (err) {
                this.setState({
                    message: 'Error happen while reset your password.',
                    loading: false
                })
                console.log(err);
            }   
        } else this.setState({ submitted: true})
    }


    render() {
        const {password, token, submitted} = this.state
        if(token === false){
            return (
				<div className="loading-content">
					<h1>Page not found</h1>
				</div>
            )
        } else {
            if (this.state.success) return <Redirect to="/login" />
            return (
                <div className="w3-card login-box">
                    <h2>Reset Password</h2>
                    <form name="form" onSubmit={this.handleResetPassword}>
                        <div className="textbox">
                            <i className="fa fa-lock" aria-hidden="true"></i>
                            <input type="password" placeholder="New password" name="password" onChange={this.handleChange}/>
                        </div>
                        { this.state.loading && <Spinner size={20} spinnerColor={"#e84393"} spinnerWidth={4} visible={true} />}
                        <div className="help-block">{this.state.message}</div>
                        {submitted && !password && 
                            <div className="help-block">Password is required</div>
                        }
                        <input className="btn-login" type="submit" name="" value="Reset Password"/>
                    </form>
                </div>
            )
        }
    }
}
export default ResetPasswordPage