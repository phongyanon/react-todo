import React, { Component } from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import Spinner from 'react-spinner-material'

import todoAction from '../../actions/todo'

import './TodoList.sass'
import './TodoCard.sass'

class TodoItem extends Component {
    constructor(props){
        super(props)
        this.state = {
            loading: false,
        }

        this.handleViewButton = this.handleViewButton.bind(this)
        this.handleEditButton = this.handleEditButton.bind(this)
        this.handleActiveButton = this.handleActiveButton.bind(this)
        this.handleCompleteButton = this.handleCompleteButton.bind(this)
        this.openDeleteModal = this.openDeleteModal.bind(this)
    }

    sleep = (milliseconds) => { // hold time for see spinner
        return new Promise((resolve) => setTimeout(resolve, milliseconds));
    };

    handleViewButton(e){
        const { param } = e.target.dataset // get data from data-param in component
        let path = `/todo/view/${param}`
        this.props.history.push(path)
    }

    handleEditButton(e){
        const { param } = e.target.dataset // get data from data-param in component      
        let path = `/todo/edit/${param}`
        this.props.history.push(path)
    }

    openDeleteModal(e){
        const { param } = e.target.dataset
        this.props.openDeleteModal(param)
    }

    async handleActiveButton(e){
        try {
            this.setState({loading: true})
            const { param } = e.target.dataset
            await this.sleep(2000)
            await this.props.updateStatusTodo({id: param, status: 'active'})
            this.setState({loading: false})
        } catch (err) {
            console.log(err)
        }
    }

    async handleCompleteButton(e){
        try {
            this.setState({loading: true})
            const { param } = e.target.dataset
            await this.sleep(2000)
            await this.props.updateStatusTodo({id: param, status: 'completed'})
            this.setState({loading: false})
        } catch (err) {
            console.log(err)
        }
    }

    render(){
        let { viewMode, todo } = this.props
        let itemTodo
        if(viewMode === 'list'){
            itemTodo = (
                <div className={todo.status === 'active'? 
                    "card card-active": "card card-complete"}>                            
                    <p>{todo.name}</p>
                    <div className="card-action">
                        { this.state.loading && <Spinner size={20} spinnerColor={"#e84393"} spinnerWidth={4} visible={true} />}
                        {todo.status === 'active'?
                            <button className="complete-action" data-param={todo.id} onClick={this.handleCompleteButton}>Complete</button>:
                            <button className="active-action" data-param={todo.id} onClick={this.handleActiveButton}>Active</button>
                        }
                        <button className="view-action" data-param={todo.id} onClick={this.handleViewButton}>View</button>
                        <button className="edit-action" data-param={todo.id} onClick={this.handleEditButton}>Edit</button>
                        <button className="del-action"  data-param={todo.id} onClick={this.openDeleteModal}>Delete</button>
                    </div>
                </div>
            )
        } else if(viewMode === 'card'){
            itemTodo = (
                <div className={todo.status === 'active'? 
                    "card-rect card-active": "card-rect card-complete"}>           
                    <p>{todo.name}</p>
                    <span>
                    { this.state.loading && <Spinner size={20} spinnerColor={"#e84393"} spinnerWidth={4} visible={true} />}
                        {todo.description}
                    </span>
                    <div className="card-action">
                        {todo.status === 'active'?
                            <button className="complete-action" data-param={todo.id} onClick={this.handleCompleteButton}>Complete</button>:
                            <button className="active-action" data-param={todo.id} onClick={this.handleActiveButton}>Active</button>
                        }
                        <button className="view-action" data-param={todo.id} onClick={this.handleViewButton}>View</button>
                        <button className="edit-action" data-param={todo.id} onClick={this.handleEditButton}>Edit</button>
                        <button className="del-action" data-param={todo.id} onClick={this.openDeleteModal}>Delete</button>
                    </div>
                </div>
            )
        } else {
            itemTodo = (<></>)
        }
        return(<>{itemTodo}</>)
    }
}

const mapDispatchToProps = {
    updateStatusTodo: todoAction.updateStatusTodo,
    openDeleteModal: todoAction.openDeleteModal
}

export default withRouter(connect(null, mapDispatchToProps)(TodoItem))