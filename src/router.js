import React from 'react';
import { Route, Switch } from 'react-router-dom';

import PrivateRoute from './components/PrivateRoute'
import OverviewPage from './components/OverviewPage'
import LoginPage from './components/LoginPage'
import ForgetPasswordPage from './components/ForgetPasswordPage'
import RegisterPage from './components/RegisterPage'
import TodoPaper from './components/TodoPaper'
import NewTodo from './components/NewTodo'
import EditTodo from './components/EditTodo'
import HomePage from './components/HomePage'
import ResetPasswordPage from './components/ResetPasswordPage'

const Router = () => (
	<Switch>
        <PrivateRoute exact path="/todo" component={OverviewPage}/>
        <PrivateRoute exact path="/todo/new" component={NewTodo} />
        <PrivateRoute exact path="/todo/:category" component={OverviewPage}/>
        <PrivateRoute exact path="/todo/view/:id" component={TodoPaper} />
        <PrivateRoute exact path="/todo/edit/:id" component={EditTodo} />
        <Route exact path="/" component={HomePage}/>
        <Route path="/login" component={LoginPage}/>
        <Route path="/forgetPassword" component={ForgetPasswordPage}/>
        <Route exact path="/resetPassword/:token" component={ResetPasswordPage}/>
        <Route path="/register" component={RegisterPage}/>
	</Switch>
);

export default Router;
